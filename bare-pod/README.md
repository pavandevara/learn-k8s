
```
[root@mstr samples]# kubectl create -f pod-def.yml
pod/myapp-pod created
[root@mstr samples]# kubectl get deploy
No resources found in default namespace.
[root@mstr samples]# kubectl get pod
NAME        READY   STATUS              RESTARTS   AGE
myapp-pod   0/1     ContainerCreating   0          16s
[root@mstr samples]# kubectl get pod
NAME        READY   STATUS              RESTARTS   AGE
myapp-pod   0/1     ContainerCreating   0          29s
[root@mstr samples]# kubectl describe po myapp-pod
```

# Delete the pod check is it creating back?
 kubectl myapp-pod delete po