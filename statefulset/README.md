# How to use statefulsets in k8s

Bring up the k8s cluster.
run nfs-server
```bash
yum install -y nfs-utils
systemctl enable nfs-server
systemctl start nfs-server
```
# Insttructions for nfs-server
vi /etc/exports
```bash
/srv/nfs/kubedata   *(rw,sync,no_sub(rec_check,insecure))
mkdir /srv/nfs/kubedata -p
mkdir /srv/nfs/kubedata/{pv1,pv2,pv3,pv4}
chmod -R 777 /srv/nfs/kubedata
```
All set to export the nfs definitions
```bash
exportfs -rav
```
To check that exported
```bash
exportfs -v 
```
To list the exported mountpoints
```bash
showmount -e
```
On the worker node try to mount this nfs exported
```bash
mount -t nfs 192.168.33.120:/srv/nfs/kubedata /mnt
ls /mnt # expected to see the 4 folders pv0 .. pv4
umount mnt

```