# Experimenting with kubernetes Storage
Here we will be learning about different Storage options. How we can use the emphermal storage which going to destroy when pod evicted. emptyDir is allow use to create tmporary storage. It is like tmpfs in Docker Volumes. or you can compare with tmpfs in Unix.

## EmptyDir Volume 
This is temporary virtual disk created at pod level and it can be shared between the containers. Here in the following example we are going to use myjenkins, my-centos containers and the volume as scratch-vol shared volume between containers.

```bash
kubectl apply -f emptydir-multicon.yaml
kubectl get po -o wide
history
kubectl exec test-emptydir-pod -c myjenkins -- df /scratch
kubectl exec test-emptydir-pod -c my-centos -- df /scratch
kubectl exec test-emptydir-pod -c my-centos -it bash
cd scratch/
cat myjenkins.out
kubectl exec test-emptydir-pod -c myjenkins -it bash

cd scratch/
cat myjenkins.out
```

## Create pv, pvc and a pod deployment with svc
kubectl apply -f 10g-local-pv.yaml -f 3g-local-pvc.yaml -f nginx-store-sample.yaml
kubectl get po
kubectl get po -o wide
kubectl get pv
kubectl get pvc
kubectl get po -o wide

Observed that in the persistence volume definition having 
```yaml
  hostPath:
    path: "/scratch/data"
```
Now you know where the pod is created on which node.
hence, login to the corresponding node and create the /scratch/data folder.

List the directory of a nginx pod container that has the volumeMount defined `- mountPath: "/usr/share/nginx/html"`
```bash
kubectl exec my-nginx-6bcb9cd877-tfss9 --  ls /usr/share/nginx/html
```
Let's add a file(s) with touch command on that nginx pod.
kubectl exec my-nginx-6bcb9cd877-tfss9 --  touch /usr/share/nginx/html/test1

On the node1 box and check that pv defined with  
kubectl exec my-nginx-6bcb9cd877-tfss9 --  touch /usr/share/nginx/html/test2
kubectl exec my-nginx-6bcb9cd877-tfss9 --  echo Namaste > /usr/share/nginx/html/test3

Go to node1 and try to append a line in the test2 file. Check if that reflects in the pod container

kubectl exec my-nginx-6bcb9cd877-tfss9 --  cat /usr/share/nginx/html/test2

This confirms that the data will be persist on the host machine. Which can be shared with other pods which are created on the same node.  
```bash
kubectl get deploy
kubectl scale replicas=3 deploy my-nginx
kubectl scale --replicas=3 deploy my-nginx
kubectl get all
kubectl get po -o wide
kubectl describe po my-nginx-6bcb9cd877-r4bdm
kubectl get po -o wide
kubectl exec my-nginx-6bcb9cd877-p4mds --  cat /usr/share/nginx/html/test2
kubectl exec my-nginx-6bcb9cd877-tfss9 --  cat /usr/share/nginx/html/test2
```
But remember that node1 contained files cannot be accessable in the pod which is created on node2. 
```bash
kubectl exec my-nginx-6bcb9cd877-r4bdm --  cat /usr/share/nginx/html/test2
```
## Cleanup process
First delete the deployment
```bash
kubectl get deploy
kubectl delete  deploy my-nginx
```
Note that in the ndoe1 still the files stored even when pv, pvc and pods are removed.
```bash
cd /scratch/data && ls -l
```
kubectl get deploy
```
Delete the pv-claim
kubectl get pvc
kubectl delete  pvc pv-claim
kubectl get pvc

Now clean the pv also...
```bash
 kubectl get pv
  kubectl delete  pv pv-volume
 kubectl get pv  
 ```