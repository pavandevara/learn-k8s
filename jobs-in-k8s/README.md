# Job-controller in Kubernetes

## Job experiment
Working with namaste-batch-job.yaml

In this hands-on section we will be experimenting with the Job and its attributes.

```bash
kubectl get all
kubectl create -f namaste-batch-job.yaml
kubectl get all
kubectl describe job.batch/namastejob
```
in the describe option look for the Pods Statuses, Start Time, Completed At, Events

Modify the the yaml file  by adding the 
1. completions: 2 under the spec
2. parallelism: 2

Check the completion value in the describe output for the first change
Check the 'Duration' value in thee describe output
```bash
kubectl describe job.batch/namastejob
```
### Cleanup the job
```bash
kubectl get all
kubectl delete -f namaste-batch-job.yaml
kubectl get all
```

## CronJob experiment
Using cronjob-namaste.yaml

```bash
kubectl get all
kubectl create -f cronjob-namaste.yaml
kubectl get all
kubectl describe job.batch/namastejob
```

### Cleanup the CronJob
```bash
kubectl get all
kubectl delete -f cronjob-namaste.yaml

kubectl get all
```