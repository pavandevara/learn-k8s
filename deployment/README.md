vi mydeployment.yml
# Using deployment
kubectl apply -f mydeployment.yml

# Check the deployment with following 
kubectl get deploy
kubectl get rs
kubectl get po

# See the values Replicas, Pod Template:,  Events
kubectl describe deploy mynginx-deploy
kubectl get po

# Upgrade with new version of image
kubectl set image deployment mynginx-deploy mynginx-container=nginx:1.12.0 --record
kubectl get po
kubectl describe deploy mynginx-deploy
kubectl rollout history deployment mynginx-deploy
kubectl set image deployment mynginx mynginx=nginx:1.12.0 --record

# another upgrade
kubectl set image deployment mynginx-deploy mynginx-container=nginx:1.17 --record

# Check the REVISION
kubectl rollout history deployment mynginx-deploy
kubectl describe deploy mynginx-deploy
kubectl get po
kubectl get rs

# Rollback to previous version
kubectl rollout undo deployment mynginx-deploy --to-revision=2
kubectl get rs
kubectl get deploy
kubectl get po
kubectl describe deploy mynginx-deploy

# Scale up the pods to 6 from the initial count
kubectl scale deploy mynginx-deploy --replicas=6

Enjoy the fun!