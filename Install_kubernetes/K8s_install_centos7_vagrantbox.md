# Kubernetes Installation and configuration steps

## 1.Prerequisites
------------------
Minimum requirements for your practice lab

2 GB or more of RAM per machine
2 CPUs or more

To get the above minimum prerequisites for Vagrant box in the Vagrantfile add the below:
```ruby
config.vm.provider "virtualbox" do |vb|
    vb.memory = "4096"
	vb.customize ["modifyvm", :id, "--ioapic", "on"]
	vb.customize ["modifyvm", :id, "--cpus", "2"] 
   end
```   
   
## Network requirements

Full network connectivity between all machines in the cluster
Unique hostname, MAC address
Certain ports for k8s components are open on your machines
Swap disabled. You MUST disable swap in order for the kubelet to work properly.

Kubernetes Cluster setup build with the following:
1 for master/Head node
2 for worker nodes
Total 3 VMs 

3. Update the hosts file to interconnect each other VM
vi /etc/hosts   --- in all nodes

<master-ip> k8s-master
<node01-ip>  node01
<node02-ip>  node02

4. Disable SELinux configuration for docker.

setenforce 0
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux

5. Enable br_netfilter Kernel Module
For RHEL/CentOS extra step
The br_netfilter module is required for kubernetes installation. 
Enable this kernel module so that the packets traversing the bridge are processed by iptables for filtering and for port forwarding, and the kubernetes pods across the cluster can communicate with each other.
Run the command below to enable the br_netfilter kernel module.

```bash
modprobe br_netfilter
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
```
6.Disable SWAP

Disable SWAP for kubernetes installation by running the following commands.
```bash
swapoff -a
```
And then edit the '/etc/fstab' file.
vi /etc/fstab
Comment using # inside the fstab file where you see  swap defined line

7. Install dockerce

8. Add k8s repo to yum reposiroties
```bash
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
        https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

ls -l /etc/yum.repos.d/kubernetes.repo
# Check the content of the file
less /etc/yum.repos.d/kubernetes.repo
```
9. Install the kubernetes packages kubeadm, kubelet, and kubectl 
```bash
yum install -y kubelet kubeadm kubectl
```
10. Reboot all the machines.
```bash
reboot
```

11.Relogin to the machines and start docker and kubelet.
```bash
systemctl start docker && systemctl enable docker
systemctl start kubelet && systemctl enable kubelet
```

12. Change the cgroup-driver
We need to make sure the docker-ce and kubernetes are using same 'cgroup'.
Check docker cgroup using the docker info command.
```bash
docker info | grep -i cgroup
```
And you see the docker is using 'cgroupfs' as a cgroup-driver.

Now run the command below to change the kuberetes cgroup-driver to 'cgroupfs'.
sed -i 's/cgroup-driver=systemd/cgroup-driver=cgroupfs/g' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
Reload the systemd system and restart the kubelet service.
```bash
systemctl daemon-reload
systemctl restart kubelet
```
13. On master Node modify the master node IP as per your machine
```bash
kubeadm init --apiserver-advertise-address=199.200.200.206 --pod-network-cidr=10.244.0.0/16
```
For other user access...
```bash
cp /etc/kubernetes/admin.conf /tmp
chmod 755 /tmp/admin.conf

Now login with docker user

mkdir -p $HOME/.kube
cp /tmp/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
```
Check the node list
```bash
kubectl get nodes
```
which shows master node but 'NotReady' STATUS. To resolve this we need to use pod-network setup with the following step.

14.Deploy the flannel network to the kubernetes cluster using the kubectl command.
```bash
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```
15. Installation post-checks
```bash
kubectl get nodes
kubectl get pods --all-namespaces
```
16. Adding other nodes to the cluster using kubeadm join disaplyed on the output of master node.
```bash
kubeadm join <masterip>:6443 --token <token-name> --discovery-token-ca-cert-hash sha256:<tokenhash>

kubeadm join 199.200.200.206:6443 --token g5b0e3.22oypatwr0m8uqa3 --discovery-token-ca-cert-hash sha256:fb423fd3ecfcad570ae933a6198253078d2c92855beb146adffee2e8f757df99
```
17. Testing a sample Nginx deployment
```bash

kubectl create deployment mynginx --image=nginx
kubectl describe deployment mynginx
kubectl scale --replicas=3 deployment/mynginx
kubectl create service nodeport mynginx --tcp=8080:80
```
note:- The service name and deployment name in the above commands should match.
```bash
kubectl get pods
kubectl get pods –o wide

curl node01:30741
curl node02:30741
```
Have fun in learning Kubernetes on your own laptop!!
