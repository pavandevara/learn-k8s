Multi containers Jenkins, CentOS into a single pod: multicon-pod

# Create multi-container pod file
vi multi-con-pod.yml

kubectl create -f  multi-con-pod.yml
kubectl get pod

Check the description to know the latest events on the pod
kubectl describe po multicon-pod

# Enter into CentOS7
kubectl exec -it multicon-pod -c my-centos bash


# Testing the Jenkins url from CentOS7 container
curl -L http://localhost:8080